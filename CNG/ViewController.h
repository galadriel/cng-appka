//
//  ViewController.h
//  CNG
//
//  Created by Petr Jelinek on 28/11/13.
//  Copyright (c) 2013 Petr Jelinek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Station.h"



@interface ViewController : UIViewController

- (Station *)createStationWith:(NSString*)title
                  withSubtitle:(NSString*)subtitle
                     withCislo:(NSString*)cislo
                    withAdresa:(NSString*)adresa
             withOteviraciDoba:(NSString*)oteviraciDoba
              withProvozovatel:(NSString*)provozovatel
                   withKontakt:(NSString*)kontakt
              withZpusobPlneni:(NSString*)zpusobPlneni
             withZpusobPlaceni:(NSString*)zpusobPlaceni
                withCoordinate:(CLLocationCoordinate2D)coordinate;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@end
