//
//  Annotation.m
//  CNG
//
//  Created by Petr Jelínek on 04/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import "Station.h"

@implementation Station
@synthesize isFav, coordinate,latitude, longitude, title, subtitle, adresa, oteviraciDoba, provozovatel, kontakt, cislo, zpusobPlneni, zpusobPlaceni, originalAnnotations, cluster;

- (NSArray *)originalAnnotations {
    NSAssert(self.cluster != nil, @"This annotation should have a cluster assigned!");
    return self.cluster.originalAnnotations;
}

- (void)encodeWithCoder:(NSCoder *)coder;
{
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    
    [coder encodeBool:isFav forKey:@"isFav"];
    [coder encodeInteger:latitude forKey:@"latitude"];
    [coder encodeInteger:longitude forKey:@"longitude"];
    [coder encodeInteger:cislo forKey:@"cislo"];
    [coder encodeObject:title forKey:@"title"];
    [coder encodeObject:subtitle forKey:@"subtitle"];
    [coder encodeObject:adresa forKey:@"adresa"];
    [coder encodeObject:oteviraciDoba forKey:@"oteviraciDoba"];
    [coder encodeObject:provozovatel forKey:@"provozovatel"];
    [coder encodeObject:kontakt forKey:@"kontakt"];
    [coder encodeObject:zpusobPlaceni forKey:@"zpusobPlaceni"];
    [coder encodeObject:zpusobPlneni forKey:@"zpusobPlneni"];
}


- (id)initWithCoder:(NSCoder *)coder;
{
    self = [[Station alloc] init];
    if (self != nil)
    {
        isFav = [coder decodeBoolForKey:@"isFav"];
        latitude = [coder decodeIntegerForKey:@"latitude"];
        longitude = [coder decodeIntegerForKey:@"longitude"];
        coordinate.latitude = latitude;
        coordinate.longitude = longitude;
        title = [coder decodeObjectForKey:@"title"];
        subtitle = [coder decodeObjectForKey:@"subtitle"];
        cislo = [coder decodeIntegerForKey:@"cislo"];
        adresa = [coder decodeObjectForKey:@"adresa"];
        oteviraciDoba = [coder decodeObjectForKey:@"oteviraciDoba"];
        provozovatel = [coder decodeObjectForKey:@"provozovatel"];
        kontakt = [coder decodeObjectForKey:@"kontakt"];
        zpusobPlaceni = [coder decodeObjectForKey:@"zpusobPlaceni"];
        zpusobPlneni = [coder decodeObjectForKey:@"zpusobPlneni"];
        
    }
    return self;
}


@end
