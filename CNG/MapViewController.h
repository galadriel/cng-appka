//
//  ViewController.h
//  CNG
//
//  Created by Petr Jelinek on 28/11/13.
//  Copyright (c) 2013 Petr Jelinek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Station.h"
#import <MapKit/MKAnnotation.h>
#import "ADClusterMapView.h"

extern NSInteger selectedDetailId;
extern NSMutableArray *arrayOfStations;
extern NSInteger lastView;
extern NSMutableArray *arrayOfCoords;

@interface MapViewController : UIViewController <MKMapViewDelegate>

- (NSMutableArray*)getStations;
- (NSMutableArray*)loadMasterArray;
- (NSMutableArray*)getFavourites;
- (void)saveMasterArray;
- (void)setFav;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

- (Station *)initaStation:(Station*)station
               withTitle:(NSString*)titleX
            withSubtitle:(NSString*)subtitleX
               withNumber:(NSInteger)numberX
              withAddresss:(NSString*)addressX
       withOpenHours:(NSString*)openHoursX
        withOwner:(NSString*)ownerX
             withContact:(NSString*)contactX
        withFilling:(NSString*)fillingX
       withPayment:(NSString*)paymentX
          withCoordinate:(CLLocationCoordinate2D)coordinateX;



@end
