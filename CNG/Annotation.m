//
//  Annotation.m
//  CNG
//
//  Created by Petr Jelínek on 04/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation
@synthesize coordinate, title, subtitle;

@end
