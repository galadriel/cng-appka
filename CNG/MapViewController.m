//
//  ViewController.m
//  CNG
//
//  Created by Petr Jelinek on 28/11/13.
//  Copyright (c) 2013 Petr Jelinek. All rights reserved.
//

#import "MapViewController.h"
#import "Station.h"
#import "DetailViewController.h"



@interface MapViewController ()

@end

NSInteger selectedDetailId;
NSMutableArray *arrayOfStations;
NSInteger lastView;
NSMutableArray *arrayOfCoords;

@implementation MapViewController

@synthesize mapView;

- (NSMutableArray*)getStations
{
  //  NSLog(@"POLE STANIC PRED ODESLANIM COUNT:   %lo", poleStanic.count);
    return arrayOfStations;
}

- (NSMutableArray*)getFavourites
{
    NSMutableArray *arrayOfFavs = [[NSMutableArray alloc] init];
    
    for (int x=0; x < [arrayOfStations count]; x++)
    {
        Station *station = [arrayOfStations objectAtIndex:x];
        if (station.isFav == YES)
        {
            [arrayOfFavs addObject:station];
        }

    }
    return arrayOfFavs;
}

- (void)setFav
{
    Station *station = [arrayOfStations objectAtIndex:selectedDetailId-1];
    station.isFav = YES;
    [arrayOfStations replaceObjectAtIndex:selectedDetailId-1 withObject:station];
    [self saveMasterArray];
}


-(void)saveMasterArray
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:arrayOfStations] forKey:@"arrayOfStations"];
}

- (NSMutableArray*)loadMasterArray
{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:@"arrayOfStations"];
    if (dataRepresentingSavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray != nil)
            arrayOfStations = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        else
            arrayOfStations = [[NSMutableArray alloc] init];
    }
    return arrayOfStations;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lastView = 1;
    
    // vytvoří stanice a přidá na mapu
    arrayOfStations = [[NSMutableArray alloc] init];
    //poleCoord = [[NSMutableArray alloc] init];
    
    Station * station1 = [Station alloc];
    CLLocationCoordinate2D coord;
    coord.latitude = 50.177474;
    coord.longitude = 14.671795;
    station1 = [self initaStation:station1 withTitle:@"BONETT EUROGAS" withSubtitle:@"Brandýs nad Labem" withNumber:1 withAddresss:@"Zápská 1855" withOpenHours:@"NONSTOP" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 252 767" withFilling:@"samoobslužné, stojan 2 x NGV 1" withPayment:@"karta CNG, Bonett Eurogas, CCS, Visa, cash" withCoordinate:coord];
    [arrayOfStations addObject:station1];
//    [self.mapView addAnnotation:station1];
    
    Station * station2 = [Station alloc];
    coord.latitude = 49.184332;
    coord.longitude = 16.673887;
    station2 = [self initaStation:station2 withTitle:@"RWE ENERGO" withSubtitle:@"Brno-Slatina" withNumber:2 withAddresss:@"Hviezdoslavova 1276/1" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo s.r.o." withContact:@"+420 724 167 750" withFilling:@"NGV 1" withPayment:@"karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station2];
 //    [self.mapView addAnnotation:station2];
    
    Station * station3 = [Station alloc];
    coord.latitude = 49.165532;
    coord.longitude = 16.6297473;
    station3 = [self initaStation:station3 withTitle:@"E-ON ENERGIE" withSubtitle:@"Brno-Kaštanová" withNumber:3 withAddresss:@"Kaštanová" withOpenHours:@"NONSTOP" withOwner:@"E-ON Energie, a.s." withContact:@"+420 724 258 723" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station3];
 //    [self.mapView addAnnotation:station3];
    
    Station * station4 = [Station alloc];
    coord.latitude = 50.67751312;
    coord.longitude = 14.53635406;
    station4 = [self initaStation:station4 withTitle:@"RWE ENERGO" withSubtitle:@"Česká Lípa" withNumber:4 withAddresss:@"Konopeova" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo s.r.o." withContact:@"+420 721 868 133" withFilling:@"nevyplněno" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station4];
//     [self.mapView addAnnotation:station4];
    
    Station * station5 = [Station alloc];
    coord.latitude = 48.97563171;
    coord.longitude = 14.49112511;
    station5 = [self initaStation:station5 withTitle:@"E-ON ENERGIE" withSubtitle:@"České Budějovice" withNumber:5 withAddresss:@"Vrbenská 2" withOpenHours:@"NONSTOP" withOwner:@"E-ON Energie, a.s." withContact:@"+420 724 258 723" withFilling:@"nevyplněno" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station5];
//     [self.mapView addAnnotation:station5];
    
    Station * station6 = [Station alloc];
    coord.latitude = 49.67618561;
    coord.longitude = 18.37313843;
    station6 = [self initaStation:station6 withTitle:@"GASCONTROL" withSubtitle:@"Frýdek-Místek" withNumber:6 withAddresss:@"Slezská" withOpenHours:@"Po-Pá 6:15-19:30, So-Ne 7:15-18:30" withOwner:@"Gascontrol, s.r.o." withContact:@"+420 558 632 400" withFilling:@"italský typ, redukce" withPayment:@"karta CNG, CCS, cash" withCoordinate:coord];
    [arrayOfStations addObject:station6];
 //    [self.mapView addAnnotation:station6];
    
    Station * station7 = [Station alloc];
    coord.latitude = 49.78921509;
    coord.longitude = 18.44703102;
    station7 = [self initaStation:station7 withTitle:@"GASCONTROL" withSubtitle:@"Havířov" withNumber:7 withAddresss:@"Dělnická 46" withOpenHours:@"Po-Pá 7:00-17:00" withOwner:@"Gascontrol, s.r.o." withContact:@"+420 724 519 653" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station7];
 //    [self.mapView addAnnotation:station7];
    
    Station * station8 = [Station alloc];
    coord.latitude = 50.20956421;
    coord.longitude = 15.80693436;
    station8 = [self initaStation:station8 withTitle:@"RWE ENERGO" withSubtitle:@"Hradec Králové" withNumber:8 withAddresss:@"Pražská 485" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo s.r.o." withContact:@"+420 721 868 133" withFilling:@"samoobslužné" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station8];
//     [self.mapView addAnnotation:station8];
    
    Station * station9 = [Station alloc];
    coord.latitude = 49.90524673;
    coord.longitude = 16.20422173;
    station9 = [self initaStation:station9 withTitle:@"NOPEK" withSubtitle:@"Hrušová u Vysokého Mýta" withNumber:9 withAddresss:@"Hrušová 127" withOpenHours:@"NONSTOP" withOwner:@"Nopek, a.s." withContact:@"+420 606 643 176" withFilling:@"NGV 1" withPayment:@"karta CNG, plat. karty" withCoordinate:coord];
    [arrayOfStations addObject:station9];
 //    [self.mapView addAnnotation:station9];
    
    Station * station10 = [Station alloc];
    coord.latitude = 50.24008390;
    coord.longitude = 12.75671690;
    station10 = [self initaStation:station10 withTitle:@"BONETT EUROGAS" withSubtitle:@"Chodov" withNumber:10 withAddresss:@"Karlovarská 1073" withOpenHours:@"NONSTOP" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 252 767" withFilling:@"2xNGV 1" withPayment:@"karta CNG, Bonett Eurogas, CCS, Visa, cash" withCoordinate:coord];
    [arrayOfStations addObject:station10];
 //    [self.mapView addAnnotation:station10];
    
    Station * station11 = [Station alloc];
    coord.latitude = 50.22699356;
    coord.longitude = 17.19847107;
    station11 = [self initaStation:station11 withTitle:@"BONETT EUROGAS" withSubtitle:@"Jeseník" withNumber:11 withAddresss:@"Lipovská 1177" withOpenHours:@"Po-Ne 7:30-18:30" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 252 757" withFilling:@"samoobslužné" withPayment:@"karta CNG, Bonett Eurogas, CCS, Visa, cash" withCoordinate:coord];
    [arrayOfStations addObject:station11];
 //    [self.mapView addAnnotation:station11];
    
    Station * station12 = [Station alloc];
    coord.latitude = 49.38612470;
    coord.longitude = 15.60009440;
    station12 = [self initaStation:station12 withTitle:@"DP MĚSTA JIHLAVA" withSubtitle:@"Jihlava" withNumber:12 withAddresss:@"Brtnická 23" withOpenHours:@"NONSTOP" withOwner:@"Dopravní podnik města Jihlava, a.s." withContact:@"nevyplněno" withFilling:@"samoobslužné" withPayment:@"plat. karty, karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station12];
  //   [self.mapView addAnnotation:station12];
    
    Station * station13 = [Station alloc];
    coord.latitude = 49.15955890;
    coord.longitude = 15.02278890;
    station13 = [self initaStation:station13 withTitle:@"ZLINER ENERGY" withSubtitle:@"Jindřichův Hradec" withNumber:13 withAddresss:@"Dolní Skrýchov 59" withOpenHours:@"NONSTOP" withOwner:@"Zliner Energy, a.s." withContact:@"+420 605 527 390" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station13];
  //   [self.mapView addAnnotation:station13];
    
    Station * station14 = [Station alloc];
    coord.latitude = 50.24026108;
    coord.longitude = 12.89621925;
    station14 = [self initaStation:station14 withTitle:@"RWE ENERGO" withSubtitle:@"Karlovy Vary" withNumber:14 withAddresss:@"Sportovní 1" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo, s.r.o." withContact:@"+420 606 632 624" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station14];
  //   [self.mapView addAnnotation:station14];
    
    Station * station15 = [Station alloc];
    coord.latitude = 50.13039900;
    coord.longitude = 14.10399800;
    station15 = [self initaStation:station15 withTitle:@"BONETT EUROGAS" withSubtitle:@"Kladno" withNumber:15 withAddresss:@"Železničářů 885" withOpenHours:@"NONSTOP" withOwner:@"Bonett EUROGAS Kladno, a.s." withContact:@"+420 737 252 767" withFilling:@"NGV 1" withPayment:@"karta CNG, Bonett Eurogas, CCS, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station15];
  //   [self.mapView addAnnotation:station15];
    
    Station * station16 = [Station alloc];
    coord.latitude = 49.93330200;
    coord.longitude = 14.01956300;
    station16 = [self initaStation:station16 withTitle:@"VEMEX - LUKOIL" withSubtitle:@"Králův Dvůr" withNumber:16 withAddresss:@"Pod Dálnicí 326" withOpenHours:@"NONSTOP" withOwner:@"VEMEX, s.r.o. - LUKOIL ČR, s.r.o." withContact:@"+420 603 716 492" withFilling:@"NGV 1" withPayment:@"plat. karty, LUKOIT Fleet Card, cash" withCoordinate:coord];
    [arrayOfStations addObject:station16];
 //    [self.mapView addAnnotation:station16];

    Station * station17 = [Station alloc];
    coord.latitude = 50.74481964;
    coord.longitude = 15.05226517;
    station17 = [self initaStation:station17 withTitle:@"BONETT EUROGAS" withSubtitle:@"Liberec" withNumber:17 withAddresss:@"České mládeže 549/33" withOpenHours:@"NONSTOP" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 252 767" withFilling:@"samoobslužné" withPayment:@"karta CNG, Bonett Eurogas, CCS, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station17];
 //    [self.mapView addAnnotation:station17];
    
    Station * station18 = [Station alloc];
    coord.latitude = 50.35603100;
    coord.longitude = 13.78005900;
    station18 = [self initaStation:station18 withTitle:@"RWE ENERGO" withSubtitle:@"Louny" withNumber:18 withAddresss:@"Zeměšská" withOpenHours:@"Po-Pá 6:00-18:00" withOwner:@"RWE Energo, s.r.o." withContact:@"+420 606 632 624" withFilling:@"nevyplněno" withPayment:@"karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station18];
 //    [self.mapView addAnnotation:station18];
    
    Station * station19 = [Station alloc];
    coord.latitude = 50.23095703;
    coord.longitude = 14.85149574;
    station19 = [self initaStation:station19 withTitle:@"PAVEL ŠVESTKA" withSubtitle:@"Milovice nad Labem" withNumber:19 withAddresss:@"Armádní 863" withOpenHours:@"NONSTOP" withOwner:@"Pavel Švestka, s.r.o." withContact:@"+420 736 620 300" withFilling:@"NGV 1" withPayment:@"plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station19];
 //    [self.mapView addAnnotation:station19];
    
    Station * station20 = [Station alloc];
    coord.latitude = 50.41562271;
    coord.longitude = 14.92383957;
    station20 = [self initaStation:station20 withTitle:@"RWE ENERGO" withSubtitle:@"Mladá Boleslav" withNumber:20 withAddresss:@"Štefánikova 1251" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo, s.r.o." withContact:@"+420 721 868 133" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station20];
  //   [self.mapView addAnnotation:station20];
    
    Station * station21 = [Station alloc];
    coord.latitude = 49.58968800;
    coord.longitude = 17.29307900;
    station21 = [self initaStation:station21 withTitle:@"VEMEX - LUKOIL" withSubtitle:@"Olomouc - Lukoil" withNumber:21 withAddresss:@"Lipenská 52" withOpenHours:@"NONSTOP" withOwner:@"VEMEX, s.r.o. - LUKOIL ČR, s.r.o." withContact:@"+420 603 716 492" withFilling:@"NGV 1" withPayment:@"plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station21];
  //   [self.mapView addAnnotation:station21];
    
    Station * station22 = [Station alloc];
    coord.latitude = 49.58946500;
    coord.longitude = 17.29849110;
    station22 = [self initaStation:station22 withTitle:@"BONETT EUROGAS" withSubtitle:@"Olomouc - Bonett" withNumber:22 withAddresss:@"Lipenská 92" withOpenHours:@"NONSTOP" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 252 757" withFilling:@"samoobslužné" withPayment:@"karta CNG, Bonett Eurogas, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station22];
  //   [self.mapView addAnnotation:station22];
    
    Station * station23 = [Station alloc];
    coord.latitude = 49.95299500;
    coord.longitude = 17.86914890;
    station23 = [self initaStation:station23 withTitle:@"VEMEX - LUKOUL" withSubtitle:@"Opava" withNumber:23 withAddresss:@"Bruntálská 683/6" withOpenHours:@"Po-Ne 5:00-22:00" withOwner:@"VEMEX, s.r.o. - LUKOIL ČR, s.r.o." withContact:@"+420 603 716 492" withFilling:@"NGV 1" withPayment:@"plat. karty, LUKOIL Fleet Card, cash" withCoordinate:coord];
    [arrayOfStations addObject:station23];
  //   [self.mapView addAnnotation:station23];
    
    Station * station24 = [Station alloc];
    coord.latitude = 49.84392166;
    coord.longitude = 18.27855301;
    station24 = [self initaStation:station24 withTitle:@"RWE ENERGO" withSubtitle:@"Ostrava-Plynární" withNumber:24 withAddresss:@"Plynární 2748/6" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo, s.r.o." withContact:@"+420 724 167 750" withFilling:@"samoobslužné" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station24];
 //    [self.mapView addAnnotation:station24];
    
    Station * station25 = [Station alloc];
    coord.latitude = 49.80960083;
    coord.longitude = 18.25469971;
    station25 = [self initaStation:station25 withTitle:@"VÍTKOVICE DOPRAVA" withSubtitle:@"Ostrava-Vítkovice" withNumber:25 withAddresss:@"Ruská 2887/101" withOpenHours:@"NONSTOP" withOwner:@"Vítkovice Doprava, a.s." withContact:@"+420 595 952 161" withFilling:@"NGV 1" withPayment:@"plat. karty, karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station25];
 //    [self.mapView addAnnotation:station25];
    
    Station * station26 = [Station alloc];
    coord.latitude = 49.85958310;
    coord.longitude = 18.29438580;
    station26 = [self initaStation:station26 withTitle:@"VEMEX - LUKOIL" withSubtitle:@"Ostrava-Muglinov" withNumber:26 withAddresss:@"Bohumínská" withOpenHours:@"Po-Ne 5:00-22:00" withOwner:@"VEMEX, s.r.o. - LUKOIL ČR, s.r.o." withContact:@"+420 603 716 492" withFilling:@"NGV 1" withPayment:@"plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station26];
  //   [self.mapView addAnnotation:station26];
    
    Station * station27 = [Station alloc];
    coord.latitude = 50.02824402;
    coord.longitude = 15.75612164;
    station27 = [self initaStation:station27 withTitle:@"DP MĚSTA PARDUBICE" withSubtitle:@"Pardubice" withNumber:27 withAddresss:@"Teplého 2141" withOpenHours:@"NONSTOP" withOwner:@"Dopravní podnik města Pardubice, a.s." withContact:@"+420 466 899 289" withFilling:@"NGV 1" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station27];
 //    [self.mapView addAnnotation:station27];
    
    Station * station28 = [Station alloc];
    coord.latitude = 49.30434000;
    coord.longitude = 14.13424250;
    station28 = [self initaStation:station28 withTitle:@"ZLINER ENERGY" withSubtitle:@"Písek" withNumber:28 withAddresss:@"U Vodárny 1036" withOpenHours:@"NONSTOP" withOwner:@"Zliner Energy, a.s." withContact:@"+420 605 527 390" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, plat. karty" withCoordinate:coord];
    [arrayOfStations addObject:station28];
  //   [self.mapView addAnnotation:station28];
    
    Station * station29 = [Station alloc];
    coord.latitude = 49.73444366;
    coord.longitude = 13.37790966;
    station29 = [self initaStation:station29 withTitle:@"RWE ENERGO" withSubtitle:@"Plzeň-Doudlevecká" withNumber:29 withAddresss:@"Doudlevecká 48" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo, s.r.o." withContact:@"+420 606 632 624" withFilling:@"NGV 1" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station29];
 //    [self.mapView addAnnotation:station29];
    
    Station * station30 = [Station alloc];
    coord.latitude = 49.75101390;
    coord.longitude = 13.32284610;
    station30 = [self initaStation:station30 withTitle:@"VEMEX - LUKOIL" withSubtitle:@"Plzeň-Chebská" withNumber:30 withAddresss:@"Chebská 15" withOpenHours:@"NONSTOP" withOwner:@"VEMEX, s.r.o. - LUKOIL ČR, s.r.o." withContact:@"+420 606 632 624" withFilling:@"NGV 1" withPayment:@"karta CNG, plat. karty" withCoordinate:coord];
    [arrayOfStations addObject:station30];
  //   [self.mapView addAnnotation:station30];
    
    Station * station31 = [Station alloc];
    coord.latitude = 50.09836197;
    coord.longitude = 14.50302410;
    station31 = [self initaStation:station31 withTitle:@"PRAŽSKÁ PLYNÁRENSKÁ" withSubtitle:@"Praha 9" withNumber:31 withAddresss:@"Pod Šancemi 444/1" withOpenHours:@"NONSTOP" withOwner:@"Pražská plynárenská, a.s." withContact:@"+420 606 743 698" withFilling:@"NGV 1" withPayment:@"karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station31];
  //   [self.mapView addAnnotation:station31];
    
    Station * station32 = [Station alloc];
    coord.latitude = 50.05693054;
    coord.longitude = 14.51401329;
    station32 = [self initaStation:station32 withTitle:@"PRAŽSKÁ PLYNÁRENSKÁ" withSubtitle:@"Praha 10 - Švehlova" withNumber:32 withAddresss:@"Švehlova 10" withOpenHours:@"NONSTOP" withOwner:@"Pražská plynárenská, a.s." withContact:@"+420 606 743 698" withFilling:@"NGV 1" withPayment:@"CCS, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station32];
  //   [self.mapView addAnnotation:station32];
    
    Station * station33 = [Station alloc];
    coord.latitude = 50.05577850;
    coord.longitude = 14.46280575;
    station33 = [self initaStation:station33 withTitle:@"PRAŽSKÁ PLYNÁRENSKÁ" withSubtitle:@"Praha 4 - U Plynárny" withNumber:33 withAddresss:@"U Plynárny 500" withOpenHours:@"NONSTOP" withOwner:@"Pražská plynárenská, a.s." withContact:@"+420 606 743 698" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station33];
  //   [self.mapView addAnnotation:station33];
    
    Station * station34 = [Station alloc];
    coord.latitude = 50.02153397;
    coord.longitude = 14.40414429;
    station34 = [self initaStation:station34 withTitle:@"PRAŽSKÁ PLYNÁRENSKÁ" withSubtitle:@"Praha 4 - Modřanská" withNumber:34 withAddresss:@"Modřanská" withOpenHours:@"NONSTOP" withOwner:@"Pražská plynárenská, a.s." withContact:@"+420 606 743 698" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station34];
 //   [self.mapView addAnnotation:station34];
    
    Station * station35 = [Station alloc];
    coord.latitude = 50.09412384;
    coord.longitude = 14.32800007;
    station35 = [self initaStation:station35 withTitle:@"PRAŽSKÁ PLYNÁRENSKÁ" withSubtitle:@"Praha 6" withNumber:35 withAddresss:@"Evropská" withOpenHours:@"NONSTOP" withOwner:@"Pražská plynárenská, a.s." withContact:@"+420 606 743 698" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, plat. karty" withCoordinate:coord];
    [arrayOfStations addObject:station35];
  //   [self.mapView addAnnotation:station35];
    
    Station * station36 = [Station alloc];
    coord.latitude = 50.05179310;
    coord.longitude = 14.35628920;
    station36 = [self initaStation:station36 withTitle:@"CIML" withSubtitle:@"Praha 5" withNumber:36 withAddresss:@"Řeporyjská 1" withOpenHours:@"NONSTOP" withOwner:@"Ciml, s.r.o." withContact:@"+420 777 914 714" withFilling:@"2xNGV 1" withPayment:@"karta CNG, plat. karty" withCoordinate:coord];
    [arrayOfStations addObject:station36];
  //   [self.mapView addAnnotation:station36];
    
    Station * station37 = [Station alloc];
    coord.latitude = 50.08368690;
    coord.longitude = 14.44301610;
    station37 = [self initaStation:station37 withTitle:@"BONETT EUROGAS" withSubtitle:@"Praha 3" withNumber:37 withAddresss:@"U Rajské zahrady 114" withOpenHours:@"NONSTOP" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 262 767" withFilling:@"NGV 1" withPayment:@"karta BONETT EUROGAS, karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station37];
 //    [self.mapView addAnnotation:station37];
    
    Station * station38 = [Station alloc];
    coord.latitude = 50.07746780;
    coord.longitude = 14.50973390;
    station38 = [self initaStation:station38 withTitle:@"VEMEX - LUKOIL" withSubtitle:@"Praha 10 - Černokostelecká" withNumber:38 withAddresss:@"Černokostelecká 1168/90" withOpenHours:@"NONSTOP" withOwner:@"VEMEX, s.r.o. - LUKOIL ČR, s.r.o." withContact:@"+420 736 484 483" withFilling:@"NGV 1" withPayment:@"karta CNG, plat. karty" withCoordinate:coord];
    [arrayOfStations addObject:station38];
 //    [self.mapView addAnnotation:station38];
    
    Station * station39 = [Station alloc];
    coord.latitude = 49.46629715;
    coord.longitude = 17.13292503;
    station39 = [self initaStation:station39 withTitle:@"FTL" withSubtitle:@"Prostějov" withNumber:39 withAddresss:@"Kojetínská 1" withOpenHours:@"NONSTOP" withOwner:@"FTL Prostějov" withContact:@"+420 582 365 995" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station39];
 //    [self.mapView addAnnotation:station39];
    
    Station * station40 = [Station alloc];
    coord.latitude = 49.43634033;
    coord.longitude = 17.45914268;
    station40 = [self initaStation:station40 withTitle:@"BONETT EUROGAS" withSubtitle:@"Přerov" withNumber:40 withAddresss:@"K Moštěnici 265/8a" withOpenHours:@"NONSTOP" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 252 757" withFilling:@"NGV 1, NGV 2" withPayment:@"karta BONETT EUROGAS, karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station40];
 //    [self.mapView addAnnotation:station40];
    
    Station * station41 = [Station alloc];
    coord.latitude = 50.60789108;
    coord.longitude = 15.32063675;
    station41 = [self initaStation:station41 withTitle:@"RWE ENERGO" withSubtitle:@"Semily" withNumber:41 withAddresss:@"Na Rovinkách 211" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo, s.r.o." withContact:@"+420 721 868 133" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station41];
 //    [self.mapView addAnnotation:station41];
    
    Station * station42 = [Station alloc];
    coord.latitude = 50.61992264;
    coord.longitude = 15.82100391;
    station42 = [self initaStation:station42 withTitle:@"E-ON ENERGIE" withSubtitle:@"Svoboda nad Úpou" withNumber:42 withAddresss:@"Nádražní 501" withOpenHours:@"NONSTOP" withOwner:@"E-ON Energie, a.s." withContact:@"+420 724 258 723" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station42];
 //    [self.mapView addAnnotation:station42];
    
    Station * station43 = [Station alloc];
    coord.latitude = 49.40845490;
    coord.longitude = 14.68840313;
    station43 = [self initaStation:station43 withTitle:@"COMETT PLUS" withSubtitle:@"Tábor" withNumber:43 withAddresss:@"Chýnovská 1917" withOpenHours:@"NONSTOP" withOwner:@"COMETT PLUS, s.r.o." withContact:@"+420 737 262 292" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station43];
 //    [self.mapView addAnnotation:station43];
    
    Station * station44 = [Station alloc];
    coord.latitude = 50.50855500;
    coord.longitude = 14.14704300;
    station44 = [self initaStation:station44 withTitle:@"AUTOEXPERT" withSubtitle:@"Terezín" withNumber:44 withAddresss:@"Dukelských hrdinů 330" withOpenHours:@"NONSTOP" withOwner:@"Autoexpert, s.r.o." withContact:@"+420 605 294 357" withFilling:@"nevyplněno" withPayment:@"karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station44];
 //    [self.mapView addAnnotation:station44];
    
    Station * station45 = [Station alloc];
    coord.latitude = 49.20211411;
    coord.longitude = 15.89741802;
    station45 = [self initaStation:station45 withTitle:@"TEDOM" withSubtitle:@"Třebíč" withNumber:45 withAddresss:@"Hrotovická 160" withOpenHours:@"NONSTOP" withOwner:@"TEDOM, a.s." withContact:@"+420 725 891 099" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station45];
 //    [self.mapView addAnnotation:station45];
    
    Station * station46 = [Station alloc];
    coord.latitude = 49.05603890;
    coord.longitude = 17.46839280;
    station46 = [self initaStation:station46 withTitle:@"UHERSKÉ HRADIŠTĚ" withSubtitle:@"Uherské Hradiště" withNumber:46 withAddresss:@"Za Olšávkou 380" withOpenHours:@"NONSTOP" withOwner:@"Samospráva nemovitostí Prakšice, s.r.o." withContact:@"+420 775 371 633" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG, plat. karty, cash" withCoordinate:coord];
    [arrayOfStations addObject:station46];
 //    [self.mapView addAnnotation:station46];
    
    Station * station47 = [Station alloc];
    coord.latitude = 50.50562190;
    coord.longitude = 16.02583920;
    station47 = [self initaStation:station47 withTitle:@"E-ON ENERGIE" withSubtitle:@"Úpice" withNumber:47 withAddresss:@"Spojenců 115" withOpenHours:@"NONSTOP" withOwner:@"E-ON Energie, a.s." withContact:@"+420 724 258 723" withFilling:@"NGV 1" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station47];
  //   [self.mapView addAnnotation:station47];
    
    Station * station48 = [Station alloc];
    coord.latitude = 50.66033173;
    coord.longitude = 14.00371361;
    station48 = [self initaStation:station48 withTitle:@"RWE ENERGO" withSubtitle:@"Ústí nad Labem" withNumber:48 withAddresss:@"Textilní 6" withOpenHours:@"NONSTOP" withOwner:@"RWE Energo, s.r.o." withContact:@"+420 606 632 624" withFilling:@"NGV 1" withPayment:@"karta CNG, cash" withCoordinate:coord];
    [arrayOfStations addObject:station48];
  //   [self.mapView addAnnotation:station48];
    
    Station * station49 = [Station alloc];
    coord.latitude = 49.52200610;
    coord.longitude = 15.34762810;
    station49 = [self initaStation:station49 withTitle:@"E-ON ENERGIE" withSubtitle:@"Humpolec" withNumber:49 withAddresss:@"Vystrkov" withOpenHours:@"NONSTOP" withOwner:@"NEXT Energy, s.r.o. - E-ON Energie, a.s." withContact:@"+420 724 258 723" withFilling:@"2xNGV 1, NGV 2" withPayment:@"karta CNG, plat. karty, fleet karta, cash" withCoordinate:coord];
    [arrayOfStations addObject:station49];
 //    [self.mapView addAnnotation:station49];
    
    Station * station50 = [Station alloc];
    coord.latitude = 48.83175659;
    coord.longitude = 16.07069778;
    station50 = [self initaStation:station50 withTitle:@"EIKA" withSubtitle:@"Znojmo-Oblekovice" withNumber:50 withAddresss:@"Oblekovice 6" withOpenHours:@"NONSTOP" withOwner:@"EIKA, a.s." withContact:@"+420 737 285 191" withFilling:@"NGV 1, NGV 2" withPayment:@"plat. karty, CCS, cash" withCoordinate:coord];
    [arrayOfStations addObject:station50];
 //    [self.mapView addAnnotation:station50];
    
    Station * station51 = [Station alloc];
    coord.latitude = 48.85353470;
    coord.longitude = 16.07292938;
    station51 = [self initaStation:station51 withTitle:@"ZNOJEMSKÁ DOPRAVNÍ" withSubtitle:@"Znojmo-Dobšická" withNumber:51 withAddresss:@"Dobšická 6" withOpenHours:@"NONSTOP" withOwner:@"Znojemská dopravní spol. Psota, s.r.o." withContact:@"+420 724 313 911" withFilling:@"NGV 1, NGV 2" withPayment:@"karta CNG" withCoordinate:coord];
    [arrayOfStations addObject:station51];
 //    [self.mapView addAnnotation:station51];
    
    Station * station52 = [Station alloc];
    coord.latitude = 49.87055810;
    coord.longitude = 13.89439810;
    station52 = [self initaStation:station52 withTitle:@"BONETT EUROGAS" withSubtitle:@"Žebrák" withNumber:52 withAddresss:@"Tovární" withOpenHours:@"NONSTOP" withOwner:@"Bonett Gas Investment, a.s." withContact:@"+420 737 252 767" withFilling:@"NGV 1" withPayment:@"karta CNG, karta Bonett Eurogas, plat. karty" withCoordinate:coord];
    [arrayOfStations addObject:station52];
 //    [self.mapView addAnnotation:station52];
    
    arrayOfCoords = [[NSMutableArray alloc] initWithArray:arrayOfStations];
    
    NSArray* stations = [NSArray arrayWithArray:arrayOfStations];
    [((ADClusterMapView*)self.mapView) setAnnotations:stations];
    //[self loadMasterArray];
    
    
    
    
    //NSLog(@"POCET STANIC PO VYTVORENI:   %lo", poleStanic.count);
    
   // UIPinchGestureRecognizer* recognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self
   //                                                                                  action:@selector(handlePinch:)];
    
   // [self.mapView addGestureRecognizer:recognizer];
    
}

- (Station *)initaStation:(Station*)station
                withTitle:(NSString*)titleX
             withSubtitle:(NSString*)subtitleX
                withNumber:(NSInteger)numberX
               withAddresss:(NSString*)addressX
        withOpenHours:(NSString*)openHoursX
         withOwner:(NSString*)ownerX
              withContact:(NSString*)contactX
         withFilling:(NSString*)fillingX
        withPayment:(NSString*)paymentX
           withCoordinate:(CLLocationCoordinate2D)coordinateX
{
    station.coordinate = coordinateX;
    station.title = titleX;
    station.subtitle = subtitleX;
    station.cislo = numberX;
    station.adresa = addressX;
    station.oteviraciDoba = openHoursX;
    station.provozovatel = ownerX;
    station.kontakt = contactX;
    station.zpusobPlaceni = paymentX;
    station.zpusobPlneni = fillingX;
    station.latitude = coordinateX.latitude;
    station.longitude = coordinateX.longitude;
    
    return station;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"presentDetailVC"])
    {
        ((DetailViewController*)segue.destinationViewController).station = sender;
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    lastView = 1;
    
    ADClusterAnnotation* clusterAnnotation = view.annotation;
    [self performSegueWithIdentifier:@"presentDetailVC" sender:[clusterAnnotation.originalAnnotations firstObject]];
    
    
}

- (NSInteger)numberOfClustersInMapView:(ADClusterMapView *)mapView // default: 32
{
    return 19;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    ADClusterAnnotation* clusterAnnotation = annotation;
    
    // Handle any custom annotations.
    if (clusterAnnotation.type == ADClusterAnnotationTypeLeaf)
    {
        
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CNGLeaf"];
        
        if (pinView == nil)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CNGLeaf"];
            pinView.canShowCallout = YES;
            
            pinView.pinColor = MKPinAnnotationColorPurple;
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            pinView.rightCalloutAccessoryView = rightButton;
        } else
        {
            pinView.annotation = annotation;
        }
        
        return pinView;
        
    } else if (clusterAnnotation.type == ADClusterAnnotationTypeCluster)
    {
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CNGCluster"];
        
        if (pinView == nil)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CNGCluster"];
            pinView.canShowCallout = YES;
            
            pinView.pinColor = MKPinAnnotationColorRed;
        } else
        {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    return nil;
}


// maximální zoom out na velikost ČR
/*
- (void)handlePinch:(UIPinchGestureRecognizer*)recognizer
{
    static MKCoordinateRegion originalRegion;
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        originalRegion = self.mapView.region;
    }
    
    double latdelta = originalRegion.span.latitudeDelta / recognizer.scale;
    double londelta = originalRegion.span.longitudeDelta / recognizer.scale;
    
    // TODO: set these constants to appropriate values to set max/min zoomscale
    latdelta = MAX(MIN(latdelta, 6.88), 0.03);
    londelta = MAX(MIN(londelta, 6.88), 0.03);
    MKCoordinateSpan span = MKCoordinateSpanMake(latdelta, londelta);
    
    [self.mapView setRegion:MKCoordinateRegionMake(originalRegion.center, span) animated:YES];
}

*/
 
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// ukáže ČR
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 49.6;
    zoomLocation.longitude= 15.5;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 498896.64, 498896.64);
    
    [mapView setRegion:viewRegion animated:YES];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

@end
