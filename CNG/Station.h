//
//  Annotation.h
//  CNG
//
//  Created by Petr Jelínek on 04/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "ADClusterMapView.h"

@interface Station : NSObject <MKAnnotation, NSCoding>


@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, copy) NSString * title;
@property(nonatomic, copy) NSString * subtitle;
@property(nonatomic, assign) NSInteger cislo;
@property(nonatomic, assign) NSInteger latitude;
@property(nonatomic, assign) NSInteger longitude;
@property(nonatomic, copy) NSString * adresa;
@property(nonatomic, copy) NSString * oteviraciDoba;
@property(nonatomic, copy) NSString * provozovatel;
@property(nonatomic, copy) NSString * kontakt;
@property(nonatomic, copy) NSString * zpusobPlneni;
@property(nonatomic, copy) NSString * zpusobPlaceni;
@property(nonatomic, assign) BOOL isFav;
@property (weak, nonatomic, readonly) NSArray * originalAnnotations;
@property (nonatomic, weak) ADMapCluster * cluster;



@end
