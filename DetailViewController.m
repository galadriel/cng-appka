//
//  DetailViewController.m
//  CNG
//
//  Created by Petr Jelínek on 05/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import "DetailViewController.h"
#import "MapViewController.h"
#import "TableViewController.h"
#import "FavViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

@synthesize titleLable, subtitleLable, address, openHours, owner, contact, payment, filling;
@synthesize station = _station;

- (IBAction)navigateButtonPressed:(id)sender
{
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        Station *station = [arrayOfCoords objectAtIndex:(selectedDetailId-1)];
        
        CLLocationCoordinate2D coordinate;
        coordinate = station.coordinate;
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:@"Cíl"];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }
}

- (IBAction)addButtonPressed:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Přidat do oblíbených?" message:@"" delegate:self cancelButtonTitle:@"Storno" otherButtonTitles:@"OK", nil];
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:@"Storno"])
    {
        
    }
    else if([title isEqualToString:@"OK"])
    {
        MapViewController *mvc = [[MapViewController alloc] init];
        [mvc setFav];
    }
}

- (IBAction)backButtonPressed:(id)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //Station *station = [arrayOfStations objectAtIndex:(selectedDetailId-1)];
    if (lastView == 2)
    {
        self.station = [arrayOfStations objectAtIndex:(selectedDetailId-1)];
    }
    self.titleLable.text = self.station.title;
    self.subtitleLable.text = self.station.subtitle;
    self.address.text = self.station.adresa;
    self.openHours.text = self.station.oteviraciDoba;
    self.owner.text = self.station.provozovatel;
    self.contact.text = self.station.kontakt;
    self.filling.text = self.station.zpusobPlneni;
    self.payment.text = self.station.zpusobPlaceni;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
